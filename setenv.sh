# Usage: source setenv.sh
SCRIPT_HOME="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
echo "Script is located at ${SCRIPT_HOME}"
echo "Initialising Go environment..."
export MASTER_GOPATH=${SCRIPT_HOME}/go
export GOPATH=${MASTER_GOPATH}
export GOBIN=${MASTER_GOPATH}/bin
export GO111MODULE=auto
export GOROOT=${HOME}/go-sdk/go1.18beta2
export PATH=${GOBIN}:${GOROOT}/bin:${PATH}
echo "Run 'go env' to examine environment"
