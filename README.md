# Shared Vetted Personal Go Playground 

### Play with it ###

* git clone go-playground
* cd go-playground
* source setenv.sh
* fix missing Go SDK, if any
* cd channels/operations
* go run operations.go

Try to break and improve it
