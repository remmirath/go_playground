package main

import (
	"fmt"
	"go_playground/channels/operations/payload"
	"math/rand"
	"sync"
	"time"
)

func receive(wg *sync.WaitGroup, c chan payload.Payload, totalReceived *int) {
	defer func() { println("receiver done"); wg.Done() }()
	nItems := 0
	for p := range c {
		nItems++
		//time.Sleep(time.Duration(7+rand.Intn(10)) * time.Millisecond)
		fmt.Printf("received: %s, total received: %d\n", p.String(), nItems)
	}
	*totalReceived = nItems
}

func send(wg *sync.WaitGroup, done *sync.WaitGroup, sendItems int, c chan payload.Payload, id payload.Id) {
	defer wg.Done()
	defer func() { fmt.Println("sender", id, "done"); done.Done() }()
	for i := 0; i < sendItems; i++ {
		//time.Sleep(time.Duration(7+rand.Intn(10)) * time.Millisecond)
		p := *payload.New(id, i)
		fmt.Printf("sending:  %s\n", p.String())
		c <- p
	}
}

func main() {

	var wgMain sync.WaitGroup
	c1 := make(chan payload.Payload)
	wgMain.Add(2) // 1 reader and 1 closer

	var wgSendDone sync.WaitGroup

	go func(w *sync.WaitGroup) {
		defer wgMain.Done()
		w.Wait()
		close(c1)
	}(&wgSendDone)

	rand.Seed(time.Now().UnixNano())
	nSenders := rand.Intn(100) + 1 // between 1 and 100

	totalItems := 0
	for id := 1; id <= nSenders; id++ {
		wgMain.Add(1)
		wgSendDone.Add(1)
		nItems := rand.Intn(100) + 1 // between 1 and 100
		totalItems += nItems
		fmt.Printf("add sender id %d with %d items to send\n", id, nItems)
		go send(&wgMain, &wgSendDone, nItems, c1, payload.Id(id))
	}
	fmt.Printf("total senders: %d, total items to send: %d\n", nSenders, totalItems)

	totalItemsReceived := 0
	go receive(&wgMain, c1, &totalItemsReceived)

	wgMain.Wait()
	fmt.Println("======== REPORT =========")
	fmt.Printf("before: senders: %d, total items to send:  %d\n", nSenders, totalItems)
	fmt.Printf("after:  senders: %d, total items received: %d\n", nSenders, totalItemsReceived)
	if totalItems != totalItemsReceived {
		panic("totalItems != totalItemsReceived")
	}

}
