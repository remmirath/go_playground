package payload

import "fmt"

type Id int

type Payload struct {
	id Id
	i  int
}

func New(id Id, i int) *Payload {
	return &Payload{
		id: id,
		i:  i,
	}
}

func (p *Payload) String() string {
	return fmt.Sprintf("[%d, %d]", p.id, p.i)
}
